#!/bin/bash

sudo apt --assume-yes update
sudo apt --assume-yes install nginx
curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh -o install_nvm.sh
bash install_nvm.sh
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
nvm install 12.18.2
npm install -g pm2
sudo systemctl enable nginx
sudo systemctl start nginx
eval $(pm2 startup | tail -1)
sudo apt --assume-yes install zip
rm install_nvm.sh 
sudo add-apt-repository ppa:certbot/certbot -y
sudo apt --assume-yes update
sudo apt --assume-yes install python-certbot-nginx
clear