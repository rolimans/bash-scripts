#!/bin/bash
sudo apt --assume-yes update
sudo apt --assume-yes install nginx
sudo systemctl enable nginx
sudo systemctl start nginx
sudo apt --assume-yes install zip
sudo add-apt-repository ppa:certbot/certbot -y
sudo apt --assume-yes update
sudo apt --assume-yes install python-certbot-nginx
clear
